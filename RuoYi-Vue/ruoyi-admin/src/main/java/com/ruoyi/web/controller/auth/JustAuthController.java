package com.ruoyi.web.controller.auth;

import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.framework.web.service.SysLoginService;
import me.zhyd.oauth.config.AuthConfig;
import me.zhyd.oauth.model.AuthCallback;
import me.zhyd.oauth.model.AuthResponse;
import me.zhyd.oauth.request.AuthGiteeRequest;
import me.zhyd.oauth.request.AuthRequest;
import me.zhyd.oauth.utils.AuthStateUtils;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class JustAuthController {
	private final SysLoginService loginService;

	/**
	 * 获取授权链接并跳转到第三方授权页面
	 * http://localhost:8080/auth/render/gitee
	 *
	 * @param response response
	 * @throws IOException response可能存在的异常
	 */
	@RequestMapping("/render/{source}")
	public AjaxResult renderAuth(HttpServletResponse response) throws IOException {
		AuthRequest authRequest = getAuthRequest();
		String authorizeUrl = authRequest.authorize(AuthStateUtils.createState());
//		response.sendRedirect(authorizeUrl);
		return new AjaxResult(302, "redirect", authorizeUrl);
	}

	/**
	 * 用户在确认第三方平台授权（登录）后， 第三方平台会重定向到该地址，并携带code、state等参数
	 *
	 * @param callback 第三方回调时的入参
	 * @return 第三方平台的用户信息
	 */
	@RequestMapping("/callback/{source}")
	public Object login(@RequestBody AuthCallback callback) {
		AuthRequest authRequest = getAuthRequest();
		AuthResponse authResponse = authRequest.login(callback);

		AjaxResult ajax = AjaxResult.success();
		// 生成令牌
		String token = loginService.oauthLogin(authResponse);
		ajax.put(Constants.TOKEN, token);
		return ajax;
//		return authResponse;
	}

	/**
	 * 获取授权Request
	 *
	 * @return AuthRequest
	 */
	private AuthRequest getAuthRequest() {
		// fixme 改为自己的gitee签名
		return new AuthGiteeRequest(AuthConfig.builder()
			.clientId("")
			.clientSecret("")
			// 如果这个地址和gitee 配置的不一致，直接就会报错?不能存在不一致的情况?
			// gitee可以多配，这里是选择其中一个使用
			// 前端重定向后数据如下，会携带code参数，只需要配前置
			// http://localhost/login?code=545953d339f8c4a94d0d73cde644d312f61d5cf414f6708ac8c7088690684420&state=fdd64ed54542509a74600112a59cf116
			.redirectUri("http://localhost:80/login")
			.build());
	}
}