/**
 * @param {string} str
 * @returns {Boolean}
 */
export function getUrlParam(key) {
  var url = window.location.search.substr(1);
  if (url == '') {
    return null;
  }
  var paramsArr = url.split('&');
  for (var i = 0; i < paramsArr.length; i++) {
    var combina = paramsArr[i].split("=");
    if (combina[0] == key) {
      return combina[1];
    }
  }
  return null;
}
